import Home from "../views/Index";
import NewsDtail from "@/components/Index/NewsDtail";
import Product from "../views/Product";
import Case from "../views/Case";
import CaseDetail from "@/components/CaseDetail";
import AboutUs from "../views/AboutUs";
import ContactUS from "../views/ContactUS";
import Tab from "@/components/Tab";

export default [{
    path: "/",
    redirect: '/index'
  },
  {
    name:'index',
    path: "/index",
    component: Home,
    children: [{
      path: "newsDtail",
      component: NewsDtail
    }]
  },
  {
    name: 'product',
    path: "/product",
    component: Product
  },
  {
    name: 'case',
    path: "/case",
    component: Case,
    children: [{
      name: 'caseDetail',
      path: "caseDetail",
      component: CaseDetail
    }]
  },
  {
    name: 'aboutUs',
    path: "/aboutUs",
    component: AboutUs
  },
  {
    name: 'contactUs',
    path: "/contactUs",
    component: ContactUS
  }
]
